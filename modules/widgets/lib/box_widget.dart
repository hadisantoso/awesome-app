import 'package:base/base_colors.dart';
import 'package:flutter/material.dart';
class BoxWidget extends StatelessWidget {
  final Widget? child;
  final Color color;
  final EdgeInsetsGeometry padding;
  final bool showBorder;
  final double? radius;
  final bool shadow;
  BoxWidget({this.child, this.color = Colors.white, this.padding = const EdgeInsets.all(8),
    this.showBorder = false,
    this.shadow = false,
    this.radius});
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(radius ?? 12),
          color: color,
          boxShadow: [
            if(shadow)BoxShadow(
                color: BaseColors.grey,
                spreadRadius: 0.01,
                offset: Offset(0, 2),
                blurRadius: 1
            )
          ],
          border: Border.all(
              color: showBorder ? BaseColors.softGrey : Colors.transparent,
              width: 2
          )
      ),
      padding: padding,
      // width: double.infinity,
      child: child,
    );
  }
}
