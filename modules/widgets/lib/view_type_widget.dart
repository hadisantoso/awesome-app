import 'package:flutter/material.dart';

class ViewTypeWidget extends StatelessWidget {
  final double pixels;
  final double maxScrollExtent;
  final Function() onTap;
  final bool isListView;
  const ViewTypeWidget({
    required this.pixels,
    required this.maxScrollExtent,
    required this.onTap,
    required this.isListView,
    Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: Colors.black.withOpacity(0.3 * (maxScrollExtent - pixels) / maxScrollExtent),
      ),
      child: IconButton(
        icon: Icon(isListView ? Icons.grid_view : Icons.list, color: Colors.white,),
        onPressed: onTap
      ),
    );
  }
}
