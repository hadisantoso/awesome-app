import 'package:base/base.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';
import 'package:photos/model/photos_response_model.dart';
import 'package:url_launcher/url_launcher.dart';

class PhotoDetailPage extends StatelessWidget {
  final PhotoModel photo;
  const PhotoDetailPage({required this.photo, Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Container(
            key: Key("PhotoView"),
              child: PhotoView(
            imageProvider: CachedNetworkImageProvider(photo.src.large),
            loadingBuilder: (context, event) {
              return Center(
                child: CircularProgressIndicator(
                    backgroundColor: BaseColors.grey,
                    valueColor: AlwaysStoppedAnimation<Color>(BaseColors.background)),
              );
            },
            minScale: PhotoViewComputedScale.contained,
            backgroundDecoration: BoxDecoration(
              color: HexColor.fromHex(photo.avgColor),
            ),
          )),
          InkWell(
            onTap: () => Navigator.pop(context),
            child: Container(
              margin: EdgeInsets.only(top: kToolbarHeight - 18, left: 16),
              padding: EdgeInsets.all(8),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(100), color: Colors.black.withOpacity(0.3)),
              child: Icon(
                Icons.close,
                color: BlackWhiteColor.fromHex(photo.avgColor),
              ),
            ),
          ),
          Column(
            key: Key("PhotoInformation"),
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Container(
                color: BlackWhiteColor.fromHex(photo.avgColor) == Colors.white
                    ? Colors.black.withOpacity(0.3)
                    : Colors.white.withOpacity(0.3),
                padding: EdgeInsets.symmetric(vertical: 16, horizontal: 16),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            photo.photographer,
                            style: Theme.of(context)
                                .textTheme
                                .subtitle1!
                                .copyWith(color: BlackWhiteColor.fromHex(photo.avgColor)),
                          ),
                          Text(
                            photo.alt,
                            style: Theme.of(context)
                                .textTheme
                                .caption!
                                .copyWith(color: BlackWhiteColor.fromHex(photo.avgColor)),
                            maxLines: 2,
                            overflow: TextOverflow.ellipsis,
                          ),
                        ],
                      ),
                    ),
                    Container(
                      width: 8,
                    ),
                    InkWell(
                      onTap: () => launch(photo.url),
                      child: Container(
                          padding: EdgeInsets.only(left: 8, top: 8, bottom: 8),
                          child: Text(
                            "Open",
                            style: Theme.of(context)
                                .textTheme
                                .subtitle2!
                                .copyWith(color: Colors.lightBlue),
                          )),
                    )
                  ],
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}
