import 'package:dio/dio.dart';
import 'package:http_mock_adapter/http_mock_adapter.dart';

class BaseConfig{
  static Dio dio = Dio(BaseOptions(
      baseUrl: 'https://api.pexels.com/v1/',
      connectTimeout: 15000,
      receiveTimeout: 15000,
      headers: {
        'accept' : 'application/json',
        'authorization': "Bearer 563492ad6f917000010000019e8e3216421e4217bde02429715402bd"
      },
      contentType: "application/json",
      responseType: ResponseType.json));

  static int hiveCacheExpired = 60; //in minutes

  static DioAdapter dioAdapter = DioAdapter(dio: BaseConfig.dio);

  static setDioMoc(String path, int statusCode, Map<String, dynamic> response){
    BaseConfig.dioAdapter..onGet(path, (server) => server.reply(statusCode, response));
  }
}
