import 'package:flutter/material.dart';
import 'package:photo_detail/page/photo_detail_page.dart';
import 'package:photos/page/photos_page.dart';

class Routers{
  static const photos ="/photos";
  static const photo_detail ="/photo-detail";

  static Route<dynamic>? generate(RouteSettings settings) {
    switch(settings.name) {
      case photos :
        return MaterialPageRoute(builder: (_) => PhotosPage(), settings: settings);
      case photo_detail :
        dynamic args = settings.arguments;
        return MaterialPageRoute(builder: (_) => PhotoDetailPage(photo: args["photo"]), settings: settings);
    }
  }
}