import 'dart:collection';

import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:base/base.dart';
import 'package:base/base_model.dart';
import 'package:hive/hive.dart';

class HiveCache{
  static getCache(String boxName, BaseModel model) async {
    if(Platform.environment.containsKey('FLUTTER_TEST')){
      print('Flutter test hive cache : Getter Null');
      return null;
    }
    try {
      if (!(await Hive.boxExists(boxName)))
        return null;
      var box = await Hive.openBox(boxName);
      DateTime lastUpdated = box.get('last_updated');
      int diffMinutes = DateTime
          .now()
          .difference(lastUpdated)
          .inMinutes;
      LinkedHashMap? hash = box.get(boxName) as LinkedHashMap?;
      String encode = jsonEncode(hash);
      Map<String, dynamic>? decode = jsonDecode(encode);
      if (decode == null || diffMinutes > BaseConfig.hiveCacheExpired)
        return null;
      else {
        dynamic result = model.fromJsonMap(decode);
        return result;
      }
    }catch(e){
      HiveCache.deleteFromDisk(boxName);
      return null;
    }
  }

  static setCache(String boxName, dynamic data) async {
    if(Platform.environment.containsKey('FLUTTER_TEST')){
      print('Flutter test hive cache : Setter Null');
      return;
    }
    try {
      Box box = await Hive.openBox(boxName);
      await box.clear();
      box.put(boxName, data.toJson());
      box.put('last_updated',DateTime.now());
    }catch (e){
      log('Error: Hive set cache');
    }
  }

  static deleteFromDisk(String boxName) async {
    if (await Hive.boxExists(boxName)) {
      var box = await Hive.openBox(boxName);
      box.deleteFromDisk();
    }
  }

  static close() async {
    await Hive.close();
  }
}