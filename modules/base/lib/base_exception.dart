import 'dart:async';

import 'dart:io';

import 'package:dio/dio.dart';

class BaseException{
  static String errorMessage(var e, var stackTrace){
    if(!Platform.environment.containsKey('FLUTTER_TEST')) {
      Completer.sync().completeError(e, stackTrace);
    }
    try{
      if(e.error is SocketException || e.error is HandshakeException){
        return BaseExceptionMessage.noConnection;
      }
      if(e.type == DioErrorType.connectTimeout){
        return BaseExceptionMessage.connectionTimeout;
      }else{
        return BaseExceptionMessage.unknownError;
      }
    }catch(_){
      return BaseExceptionMessage.unknownError;
    }
  }
}

class BaseExceptionMessage{
  static const noConnection = "No Connection";
  static const connectionTimeout = "Connection Timeout";
  static const unknownError = "Unknown Error";
}