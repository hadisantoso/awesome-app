import 'package:flutter/material.dart';

class BaseColors {
  static const Color primary = Color(0xFF1155cc);
  static const Color background = Color(0xFFF4F4F4);
  static const Color brown = Color(0xFF8d6e63);
  static const Color grey = Color(0xFFAAAAAA);
  static const Color softGrey = Color(0xFFF4F4F4);
}

extension HexColor on Color {
  static Color fromHex(String hex) {
    final buffer = StringBuffer();
    if (hex.length == 6 || hex.length == 7) buffer.write('ff');
    buffer.write(hex.replaceFirst('#', ''));
    return Color(int.parse(buffer.toString(), radix: 16));
  }
}

extension BlackWhiteColor on Color {
  static Color fromHex(String hex){
    Color result = HexColor.fromHex(hex);
    if ((result.red*0.299 + result.green*0.587 + result.blue*0.114) > 186)
      return Colors.black;
    else
      return Colors.white;
  }
}