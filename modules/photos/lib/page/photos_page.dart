import 'dart:async';

import 'package:base/base.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:photos/bloc/photos_cubit.dart';
import 'package:photos/model/photos_response_model.dart';
import 'package:photos/page/grid_card.dart';
import 'package:photos/page/list_card.dart';
import 'package:photos/shimmer/list_view_shimmer.dart';
import 'package:photos/shimmer/photo_grid_shimmer.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:widgets/view_type_widget.dart';

class PhotosPage extends StatefulWidget {
  const PhotosPage({Key? key}) : super(key: key);

  @override
  _PhotosPageState createState() => _PhotosPageState();
}

class _PhotosPageState extends State<PhotosPage> {
  late PhotosCubit _photosCubit;
  int _page = 1;
  ScrollController _scrollController = ScrollController();
  late StreamController<double> _viewTypeStreamController;
  RefreshController _refreshController = RefreshController(initialRefresh: false);
  @override
  void initState() {
    _photosCubit = PhotosCubit();
    _viewTypeStreamController = StreamController();
    _photosCubit.getAll(page: _page);
    _scrollController.addListener(() {
      _viewTypeStreamController.sink.add(_scrollController.offset);
    });
    super.initState();
  }

  @override
  void dispose() {
    _viewTypeStreamController.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: NestedScrollView(
            controller: _scrollController,
            headerSliverBuilder: (context, bool innerBoxIsScrolled) {
              return [
                SliverAppBar(
                  expandedHeight: 200,
                  elevation: 0.0,
                  automaticallyImplyLeading: false,
                  pinned: true,
                  floating: false,
                  actions: [
                    StreamBuilder(
                        stream: _viewTypeStreamController.stream,
                        initialData: 0.0,
                        builder: (BuildContext context, AsyncSnapshot<double> snapshot) {
                          return BlocBuilder(
                              bloc: _photosCubit,
                              builder: (context, dynamic state) {
                                return ViewTypeWidget(
                                  pixels: snapshot.data!,
                                  maxScrollExtent: _scrollController.position.hasContentDimensions
                                      ? _scrollController.position.maxScrollExtent
                                      : 1.0,
                                  isListView: _photosCubit.isListView,
                                  onTap: () {
                                    _photosCubit.changeView();
                                  },
                                );
                              });
                        })
                  ],
                  backgroundColor: BaseColors.brown,
                  flexibleSpace: FlexibleSpaceBar(
                    centerTitle: true,
                    title: Text(
                      "Awesome App",
                      style: Theme.of(context).textTheme.subtitle2!.copyWith(color: Colors.white),
                    ),
                    collapseMode: CollapseMode.parallax,
                    background: Container(
                      // margin: EdgeInsets.only(top: kToolbarHeight),
                      child: BlocBuilder(
                        bloc: _photosCubit,
                        builder: (context, state) {
                          if (state is PhotosInitState) {
                            return Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                PhotoGridShimmer(
                                  height: 200 + kToolbarHeight,
                                  width: MediaQuery.of(context).size.width,
                                ),
                              ],
                            );
                          } else if (_photosCubit.data == null) {
                            return Container();
                          } else {
                            return Stack(
                              children: [
                                Row(
                                  children: [
                                    Expanded(
                                        child: CachedNetworkImage(
                                      imageUrl: _photosCubit.data!.photos[5].src.large,
                                      fit: BoxFit.cover,
                                    )),
                                  ],
                                ),
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: [
                                    Container(
                                      width: double.infinity,
                                      height: 48,
                                      color: Colors.black.withOpacity(0.3),
                                    )
                                  ],
                                )
                              ],
                            );
                          }
                        },
                      ),
                    ),
                  ),
                )
              ];
            },
            body: BlocConsumer(
              bloc: _photosCubit,
              listener: (context, dynamic state) {
                if (state is PhotosDoneState || state is PhotosFailedState)
                  _refreshController.refreshCompleted();
                if(_photosCubit.data != null && _photosCubit.errorMessage != null){
                  SnackBar snackBar = SnackBar(
                    content: Text(_photosCubit.errorMessage!),
                  );
                  ScaffoldMessenger.of(context).showSnackBar(snackBar);
                  _photosCubit.errorMessage = null;
                }
              },
              builder: (context, dynamic state) {
                return SmartRefresher(
                  enablePullDown: true,
                  onRefresh: () {
                    _page = 1;
                    _photosCubit.getAll(page: _page);
                  },
                  // scrollController: _scrollController,
                  controller: _refreshController,
                  header: MaterialClassicHeader(
                    backgroundColor: BaseColors.primary,
                  ),
                  child: _content(state, _photosCubit),
                );
              },
            )));
  }

  Widget _content(dynamic state, PhotosCubit photos) {
    if (_photosCubit.data == null && _photosCubit.errorMessage == null) {
      return Container(margin: EdgeInsets.only(top: 24), child: ListViewShimmer());
    } else if (_photosCubit.data == null && _photosCubit.errorMessage != null) {
      return Center(
        child: Text(_photosCubit.errorMessage!),
      );
    }
    _page = _photosCubit.data!.page;
    List<PhotoModel> photos = _photosCubit.data!.photos;
    if (_photosCubit.isListView) {
      return ListView.builder(
        physics: NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        padding: EdgeInsets.only(top: 16),
        itemCount: photos.isNotEmpty ? photos.length + 1 : 0,
        itemBuilder: (context, int index) {
          if (index < photos.length) {
            PhotoModel photo = photos[index];
            return Container(
              child: Container(
                  margin: EdgeInsets.only(left: 16, right: 16, bottom: 16),
                  child: InkWell(
                      onTap: () => Navigator.of(context)
                          .pushNamed(Routers.photo_detail, arguments: {"photo": photo}),
                      child: ListCard(photo: photo))),
            );
          } else {
            int tmpPage = _page + 1;
            if ((state is PhotosDoneState || state is PhotosChangeViewDoneState) &&
                tmpPage <= _photosCubit.data!.totalPages) {
              _page = tmpPage;
              _photosCubit.getAll(page: _page);
            }
            return Center(
              child: Container(
                  width: 20,
                  height: 20,
                  margin: EdgeInsets.only(top: 8, bottom: 8),
                  child: state is PhotosFetchState
                      ? CircularProgressIndicator()
                      : state is PhotosFailedState && _photosCubit.data != null
                          ? InkWell(
                              onTap: () {
                                _photosCubit.getAll(page: _page);
                              },
                              child: Icon(
                                Icons.refresh,
                                color: BaseColors.primary,
                              ),
                            )
                          : Container()),
            );
          }
        },
      );
    } else {
      return GridView.builder(
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisSpacing: 2, mainAxisSpacing: 2, childAspectRatio: 9.5 / 12, crossAxisCount: 3),
        padding: EdgeInsets.only(top: 16),
        physics: NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        itemCount: photos.isNotEmpty ? photos.length + 1 : 0,
        itemBuilder: (context, int index) {
          if (index < photos.length) {
            PhotoModel photo = photos[index];
            return InkWell(
                onTap: () => Navigator.of(context)
                    .pushNamed(Routers.photo_detail, arguments: {"photo": photo}),
                child: GridCard(photo: photo));
          } else {
            int tmpPage = _page + 1;
            if ((state is PhotosDoneState || state is PhotosChangeViewDoneState) &&
                tmpPage <= _photosCubit.data!.totalPages) {
              _page = tmpPage;
              _photosCubit.getAll(page: _page);
            }
            return Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(12), color: BaseColors.grey.withOpacity(0.3)),
              child: Center(
                child: Container(
                    width: 20,
                    height: 20,
                    margin: EdgeInsets.only(top: 8),
                    child: state is PhotosFetchState ? CircularProgressIndicator()  : state is PhotosFailedState && _photosCubit.data != null
                        ? InkWell(
                      onTap: () {
                        _photosCubit.getAll(page: _page);
                      },
                      child: Icon(
                        Icons.refresh,
                        color: BaseColors.primary,
                      ),
                    ): Container()),
              ),
            );
          }
        },
      );
    }
  }
}
