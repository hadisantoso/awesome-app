import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:photos/model/photos_response_model.dart';
import 'package:widgets/box_widget.dart';

class ListCard extends StatelessWidget {
  final PhotoModel photo;
  const ListCard({required this.photo, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BoxWidget(
        child: Row(
          children: [
            CachedNetworkImage(
              imageUrl: photo.src.medium,
              imageBuilder: (context, imageProvider) => Container(
                width: 80,
                height: 80,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(12),
                    image: DecorationImage(
                        image: imageProvider, fit: BoxFit.cover
                    )
                ),
              ),
              placeholder: (context, url) => Container(width: 80, height: 80,),
            ),
            SizedBox(
              width: 12,
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(photo.photographer, style: Theme.of(context).textTheme.subtitle2,),
                  Text(photo.alt, style: Theme.of(context).textTheme.caption,),
                ],
              ),
            )
          ],
        )
    );
  }
}
