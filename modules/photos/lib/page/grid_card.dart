import 'package:base/base.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:photos/model/photos_response_model.dart';

class GridCard extends StatelessWidget {
  final PhotoModel photo;
  const GridCard({Key? key, required this.photo}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        CachedNetworkImage(
          imageUrl: photo.src.medium,
          imageBuilder: (context, imageProvider) => Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(12),
                image: DecorationImage(
                    image: imageProvider, fit: BoxFit.cover
                )
            ),
          ),
          placeholder: (context, url) => Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(12),
              color: BaseColors.grey.withOpacity(0.3)
            ),
            height: double.infinity,
            width: double.infinity,
          ),
        ),
      ],
    );
  }
}
