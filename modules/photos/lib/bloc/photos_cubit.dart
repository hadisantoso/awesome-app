import 'package:base/base.dart';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:photos/model/photos_response_model.dart';
import 'package:photos/repository/photos_repository.dart';

part 'photos_state.dart';

class PhotosCubit extends Cubit<PhotosState> {
  PhotosCubit() : super(PhotosInitial());
  PhotosResponseModel? data;
  bool isListView = true;
  String? errorMessage;
  getAll({required int page}) async {
    try{
      PhotosResponseModel? cache;
      if(page == 1) {
        emit(PhotosInitState());
        cache = await HiveCache.getCache(this.runtimeType.toString(), PhotosResponseModel());
        if(cache != null){
          data = cache;
          emit(PhotosCacheState());
        }
      }else{
        emit(PhotosFetchState());
      }
      PhotosResponseModel result = await PhotosRepository.all(page);
      if(page == 1){
        if(cache != null) {
          PhotoModel resultFirstItem = result.photos.first;
          PhotoModel cacheFirstItem = result.photos.first;
          if(resultFirstItem.id != cacheFirstItem.id){
            data = result;
          }
        }else{
          data = result;
        }
        HiveCache.setCache(this.runtimeType.toString(), data);
      }else{
        data!.page = result.page;
        data!.photos.addAll(result.photos);
        HiveCache.setCache(this.runtimeType.toString(), data);
      }
      errorMessage = null;
      emit(PhotosDoneState());
    }catch(e, stackTrace){
      errorMessage = BaseException.errorMessage(e, stackTrace);
      emit(PhotosFailedState());
    }
  }

  changeView(){
    emit(PhotosChangeViewState());
    isListView = !isListView;
    emit(PhotosChangeViewDoneState());
  }
}