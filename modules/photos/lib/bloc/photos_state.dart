part of 'photos_cubit.dart';

@immutable
abstract class PhotosState {}

class PhotosInitial extends PhotosState {}
class PhotosInitState extends PhotosState {}
class PhotosDoneState extends PhotosState {}
class PhotosFetchState extends PhotosState {}
class PhotosFailedState extends PhotosState {}
class PhotosCacheState extends PhotosState {}
class PhotosChangeViewState extends PhotosState {}
class PhotosChangeViewDoneState extends PhotosState {}
