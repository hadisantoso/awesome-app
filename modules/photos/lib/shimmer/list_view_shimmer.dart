import 'package:flutter/material.dart';
import 'package:photos/shimmer/photo_grid_shimmer.dart';
import 'package:shimmer/shimmer.dart';
import 'package:widgets/box_widget.dart';

class ListViewShimmer extends StatelessWidget {
  const ListViewShimmer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<Widget> list = <Widget>[];
    for (var i = 0; i < 6; i++) {
      list.add(Container(
        margin: EdgeInsets.only(left: 16, right: 16, bottom: 16),
        child: BoxWidget(
            child: Shimmer.fromColors(
          baseColor: Colors.grey[300]!,
          highlightColor: Colors.grey[100]!,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                height: 80,
                width: 80,
                color: Colors.white,
              ),
              SizedBox(
                width: 12,
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      height: 12,
                      width: 80,
                      color: Colors.white,
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    Container(
                      height: 12,
                      width: 150,
                      color: Colors.white,
                    ),                  ],
                ),
              )
            ],
          ),
        )),
      ));
    }
    return SingleChildScrollView(
      child: Column(
        children: list,
      ),
    );
  }
}
