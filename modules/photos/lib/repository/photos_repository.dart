import 'package:base/base.dart';
import 'package:photos/model/photos_response_model.dart';

class PhotosRepository{
  static Future<PhotosResponseModel> all(
      int page,
      ) async {
    print(page);
    var response = await BaseConfig.dio.get("curated", queryParameters: {
      "page" : page,
      "per_page": 24
    });
    return PhotosResponseModel.fromJson(response.data);
  }
}