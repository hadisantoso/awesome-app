import 'package:base/base.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:photos/page/grid_card.dart';
import 'package:photos/page/list_card.dart';
import 'package:photos/page/photos_page.dart';
import 'package:widgets/view_type_widget.dart';

import '../mockup_data.dart';


void main(){
    testWidgets("Init list view after successfully fetch data", (WidgetTester tester) async {
      BaseConfig.setDioMoc("curated", 200, MockupData.curated);
      await tester.pumpWidget(
          MaterialApp(
            home: PhotosPage(),
          )
      );
      await tester.pumpAndSettle();
      final listCard = find.byType(ListCard);
      expect(listCard, findsWidgets);
    });

    testWidgets("Show grid view or list view when button change view is pressed", (WidgetTester tester) async {
      BaseConfig.setDioMoc("curated", 200, MockupData.curated);
      await tester.pumpWidget(
          MaterialApp(
            home: PhotosPage(),
          )
      );
      await tester.pumpAndSettle();
      final button = find.byType(ViewTypeWidget);
      await tester.tap(button);
      await tester.pumpAndSettle();
      final gridCard = find.byType(GridCard);
      expect(gridCard, findsWidgets);
      await tester.tap(button);
      await tester.pumpAndSettle();
      final listCard = find.byType(ListCard);
      expect(listCard, findsWidgets);
    });

    testWidgets("Show page error if connection error", (WidgetTester tester) async {
      BaseConfig.setDioMoc("curated", 503,{});
      await tester.pumpWidget(
          MaterialApp(
            home: PhotosPage(),
          )
      );
      await tester.pumpAndSettle();
      final text = find.text(BaseExceptionMessage.unknownError);
      expect(text, findsWidgets);
    });

    testWidgets("Open photo detail", (WidgetTester tester) async {
      BaseConfig.setDioMoc("curated", 200, MockupData.curated);
      await tester.pumpWidget(
          MaterialApp(
            initialRoute: Routers.photos,
            onGenerateRoute: Routers.generate,
          )
      );
      await tester.pumpAndSettle();
      final listCard = find.byType(ListCard);
      final firstCard = listCard.at(0);
      await tester.tap(firstCard);
      try {
        await tester.pumpAndSettle();
      }catch(_){}
      final photoView = find.byKey(Key("PhotoView"));
      final photoInformation = find.byKey(Key("PhotoInformation"));
      expect(photoView, findsOneWidget);
      expect(photoInformation, findsOneWidget);
    });
}