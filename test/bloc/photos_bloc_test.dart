import 'package:flutter_test/flutter_test.dart';
import 'package:photos/bloc/photos_cubit.dart';
import 'package:bloc_test/bloc_test.dart';

void main(){
  group('photos', (){
    blocTest<PhotosCubit, PhotosState>(
      "Get all photos page 1 from pexels.com",
      build: () => PhotosCubit(),
      act: (cubit) async => cubit.getAll(page: 1),
      expect: () => [
        isA<PhotosInitState>(),
        isA<PhotosDoneState>(),
      ]
    );
    blocTest<PhotosCubit, PhotosState>(
      "Set page to minus, its expect to be failed",
      build: () => PhotosCubit(),
      act: (cubit) async => cubit.getAll(page: -1),
      expect: () => [
        isA<PhotosFetchState>(),
        isA<PhotosFailedState>(),
      ]
    );

    blocTest<PhotosCubit, PhotosState>(
        "Change view list or grid",
        build: () => PhotosCubit(),
        act: (cubit) async => cubit.changeView(),
        expect: () => [
          isA<PhotosChangeViewState>(),
          isA<PhotosChangeViewDoneState>(),
        ]
    );
  });
}