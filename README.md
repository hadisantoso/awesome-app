# Awesome App

Awesome app is mobile application assessment from Evermos

## Features

There are some point of feature to complete this assessment.

- [x] User can choose style of the list by tapping icon in the AppBar (available options: grid  view, list view).
- [x] Please use CollapsibleAppBar for AppBar implementation.
- [x] When user scrolls down app will load more item
- [x] Retry button if load more failed
- [x] Each item should be tappable and directs user to detail page. The detail page must  contain the photo itself in a larger size and its description (photographer name and  photo URL) in a clear way. You can add the other additional information related to the  photo as you like :).

## Technical specifications.

- [x] Flutter SDK 2.8.0
- [x] Material design guidelines
- [x] Unit test and ui test
- [x] Use modular software design technique
- [x] Use BLOC state management
- [x] Layout composition use reusable widget
- [x] Git commit message clarity
- [x] Hive NoSQL for caching techniques
- [x] The App will response page with text error message if no network or error response api
