import 'package:base/routers.dart';
import 'package:base/base.dart';
import 'package:flutter/material.dart';
import 'package:hive_flutter/hive_flutter.dart';

void main() async {
  await Hive.initFlutter();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Awesome App',
      theme: ThemeData(
        primaryColor: Colors.white,
        scaffoldBackgroundColor: BaseColors.background,
        primarySwatch: Colors.blue,
        appBarTheme: AppBarTheme(
            color: BaseColors.background,
            elevation: 0.0,
        )
      ),
      onGenerateRoute: Routers.generate,
      initialRoute: Routers.photos
    );
  }
}

